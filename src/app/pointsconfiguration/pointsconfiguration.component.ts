import { Component, OnInit,Input } from '@angular/core';
import {HttpClient,HttpResponse } from "@angular/common/http";
import { Http, Response } from '@angular/http';
import {baseUrl} from '../constants';
import {CommondataService,UserInformationModel} from '../commondata.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pointsconfiguration',
  templateUrl: './pointsconfiguration.component.html',
  styleUrls: ['../assets/global/plugins/font-awesome/css/font-awesome.css','../assets/global/plugins/simple-line-icons/simple-line-icons.css','../assets/global/plugins/bootstrap/css/bootstrap.css','../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css',
  '../assets/global/plugins/select2/css/select2.css',
  '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
  '../assets/global/css/components.css',
  '../assets/global/css/plugins.css',
  '../assets/pages/css/login-5.css'
  ]
})
export class PointsconfigurationComponent implements OnInit {

  pointsCriteria : any;

  /*[
	{     criteriaName: '',     criteriaCondition: '' , criteriaValue:''  , criteriaPoints:'' },
	{     criteriaName: '',     criteriaCondition: '' , criteriaValue:''  , criteriaPoints:'' },
	{     criteriaName: '',     criteriaCondition: '' , criteriaValue:''  , criteriaPoints:'' },
	{     criteriaName: '',     criteriaCondition: '' , criteriaValue:''  , criteriaPoints:'' },
	{     criteriaName: '',     criteriaCondition: '' , criteriaValue:''  , criteriaPoints:'' }
  ]*/
  
  savePointsConfigurationUrl = baseUrl + '/savePointsConfiguration';
  getPointsConfigurationUrl = baseUrl + '/getPointsConfiguration';
  getHeaderUrl = baseUrl + '/getHeader';
  getPointsConfigurationResult: any; 
  savePointsConfigurationResult: any; 
  csvHeaders: any = [];

  constructor(private http:HttpClient,private router: Router,private commonDService:CommondataService) 
  {

  }

  ngOnInit() 
  {

    if(!this.commonDService.getLoggedInStatus())
    {
      this.router.navigateByUrl('/login');      
    }    

    this.pointsCriteria= 
    
     [
	    {     criteriaName: '',     criteriaCondition: '' , criteriaValue:''  , criteriaPoints:'' },
	    {     criteriaName: '',     criteriaCondition: '' , criteriaValue:''  , criteriaPoints:'' },
	    {     criteriaName: '',     criteriaCondition: '' , criteriaValue:''  , criteriaPoints:'' },
	    {     criteriaName: '',     criteriaCondition: '' , criteriaValue:''  , criteriaPoints:'' },
	    {     criteriaName: '',     criteriaCondition: '' , criteriaValue:''  , criteriaPoints:'' }
    ];



	  this.http.get(this.getHeaderUrl)
	  .subscribe((response: HttpResponse<any>) => 
	  {     
		//console.log(response); // logs 200     
    this.csvHeaders = Object.values(response) ; //response;
  });	     
    
    let loggedInUserInfo:UserInformationModel;
    loggedInUserInfo = this.commonDService.getUserInformation();	
	  this.getPointsConfigurationUrl = this.getPointsConfigurationUrl + "?userId="+loggedInUserInfo.user_id;
	  this.http.get(this.getPointsConfigurationUrl)
	  .subscribe((response: HttpResponse<any>) => 
	  {     
		  console.log(response); // logs 200
		  this.getPointsConfigurationResult =response ; //response;
		  if(response){
			this.pointsCriteria=response;
		  }
	  });	  
  }

  savePointsConfigurationClicked()
  {
    let loggedInUserInfo:UserInformationModel;
    loggedInUserInfo = this.commonDService.getUserInformation();    
	  var savePointsRequestPayLoad = {"savepoints": this.pointsCriteria, "userId": loggedInUserInfo.user_id};
	  var searchRequestPayLoadStringified = JSON.stringify(savePointsRequestPayLoad);
	  
	  this.http.post(this.savePointsConfigurationUrl, searchRequestPayLoadStringified)
	  .subscribe((response: HttpResponse<any>) => 
	  {  
		this.savePointsConfigurationResult = response;
		alert("Data Saved");
	  });
	  
  }  

  navigateBack()
  {
	 this.router.navigateByUrl('/searchbox');	  
  }

}
