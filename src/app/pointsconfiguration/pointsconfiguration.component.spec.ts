import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointsconfigurationComponent } from './pointsconfiguration.component';

describe('PointsconfigurationComponent', () => {
  let component: PointsconfigurationComponent;
  let fixture: ComponentFixture<PointsconfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointsconfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointsconfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
