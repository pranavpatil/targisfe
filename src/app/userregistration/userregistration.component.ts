import { Component, OnInit } from '@angular/core';
import {HttpClient,HttpResponse } from "@angular/common/http";
import { Http, Response } from '@angular/http';
import {baseUrl} from '../constants';
import { Router } from '@angular/router';
import {CommondataService} from '../commondata.service';

@Component({
  selector: 'app-userregistration',
  templateUrl: './userregistration.component.html',
styleUrls: [
  '../assets/global/plugins/font-awesome/css/font-awesome.css','../assets/global/plugins/simple-line-icons/simple-line-icons.css','../assets/global/plugins/bootstrap/css/bootstrap.css','../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css',
  '../assets/global/plugins/select2/css/select2.css',
  '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
  '../assets/global/css/components.css',
  '../assets/global/css/plugins.css',
  '../assets/pages/css/login-5.css'

  
  ]
})
export class UserregistrationComponent implements OnInit {

  user = {     username: '',     password: ''   };	
  userregistrationmessage = '';
  jsonResponse  = '';
  url = baseUrl + '/userRegistration';    

  constructor(private http:HttpClient,private router: Router,private commonDService:CommondataService) 
  { }

  ngOnInit() 
  {
  }

  userRegistrationRequest()
  {
	  if(this.isEmpty(this.user.username))
	  {
		  this.userregistrationmessage = "Please enter user name";
		  return;
	  } 
	  
	  if(this.isEmpty(this.user.password))
	  {
		  this.userregistrationmessage = "Please enter user password";
		  return;
	  } 	  
	  
	  console.log(this.user.username);
	  console.log(this.user.password);
	  var userLogin = JSON.stringify(this.user);
	  
	  //http://httpbin.org/post
	  this.http.post(this.url, userLogin)
	  .subscribe((response: HttpResponse<any>) => 
	  {     
		console.log(response); 
		
		if(response.toString()!=='SUCCESS')
		{
			this.userregistrationmessage = "User registration not successful. Please try again.";
		}
		else
		{
			this.userregistrationmessage = "User registration successful. Please communicate the user credentials to the respective user";		
		}

	  });

  }
  
 isEmpty(data) 
 {
    if(typeof(data) === 'object'){
        if(JSON.stringify(data) === '{}' || JSON.stringify(data) === '[]'){
            return true;
        }else if(!data){
            return true;
        }
        return false;
    }else if(typeof(data) === 'string'){
        if(!data.trim()){
            return true;
        }
        return false;
    }else if(typeof(data) === 'undefined'){
        return true;
    }else{
        return false;
    }
 }    

}
