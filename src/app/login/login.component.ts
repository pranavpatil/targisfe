import { Component, OnInit } from '@angular/core';
import {HttpClient,HttpResponse } from "@angular/common/http";
import { Http, Response } from '@angular/http';
import {baseUrl} from '../constants';
import { Router } from '@angular/router';
import {CommondataService,UserInformationModel} from '../commondata.service';
declare var require: any;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css','../assets/global/plugins/font-awesome/css/font-awesome.css','../assets/global/plugins/simple-line-icons/simple-line-icons.css','../assets/global/plugins/bootstrap/css/bootstrap.css','../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css',
  '../assets/global/plugins/select2/css/select2.css',
  '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
  '../assets/global/css/components.css',
  '../assets/global/css/plugins.css',
  '../assets/pages/css/login-5.css'
  ]
})

export class LoginComponent implements OnInit {
	
  logoSrc= require("../assets/images/targislogo.png");	
  loginimage= require("../assets/images/loginimage3.jpg");	  
	
  //private user = {     username: '',     password: ''   };	
  private userloginmessage = '';
  private jsonResponse  = '';
  private userLoginResponse : UserInformationModel;
  userInfoEnteredByUser : UserInformationModel = 
  {
      user_email:'',
      user_password:'',
      user_id:'',
      user_admin:'',
      user_login:'',
      user_first_name:'',
      user_last_name:''
  };
  //url = 'http://httpbin.org/post';
  private url = baseUrl + '/userLogin';

  constructor(private http:HttpClient,private router: Router,private commonDService:CommondataService) 
  { 
  }

  ngOnInit() 
  {
    this.commonDService.setLoggedInStatus(false);
  }
  
  loginRequest()
  {
	  if(this.isEmpty(this.userInfoEnteredByUser.user_email))
	  {
		this.userloginmessage = "Please enter user email";
		return;
	  } 
	  
	  if(this.isEmpty(this.userInfoEnteredByUser.user_password))
	  {
		this.userloginmessage = "Please enter user password";
		return;
	  } 	  
	  
	  var userLogin = JSON.stringify(this.userInfoEnteredByUser);
	  
	  //http://httpbin.org/post
	  this.http.post(this.url, userLogin)
	  .subscribe((response: HttpResponse<UserInformationModel>) => 
	  {
        let loginResponse: any;
        loginResponse = response;
        this.userLoginResponse = loginResponse;    

		if(this.userLoginResponse.user_login!=='SUCCESS')
		{
			this.userloginmessage = "Login Unsuccessful. Please try again.";
		}
		else
		{
            this.commonDService.setUserInformation(this.userLoginResponse);
			//localStorage.setItem("user_id", this.userLoginResponse.user_id);
            //localStorage.setItem("user_email", this.userLoginResponse.user_email);
			//localStorage.setItem("user_email", this.userLoginResponse.user_admin);            
			//localStorage.setItem("user_name", response.user_first_name + " " + response.user_last_name);
			
            this.router.navigateByUrl('/searchbox');
            this.commonDService.setLoggedInStatus(true);		
		}
	  });

  }
  
 isEmpty(data) 
 {
    if(typeof(data) === 'object'){
        if(JSON.stringify(data) === '{}' || JSON.stringify(data) === '[]'){
            return true;
        }else if(!data){
            return true;
        }
        return false;
    }else if(typeof(data) === 'string'){
        if(!data.trim()){
            return true;
        }
        return false;
    }else if(typeof(data) === 'undefined'){
        return true;
    }else{
        return false;
    }
 }  

}




