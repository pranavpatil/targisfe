import { Component, OnInit } from '@angular/core';
import { SearchpanelComponent } from '../searchpanel/searchpanel.component';
import { SearchoutputComponent } from '../searchoutput/searchoutput.component';
import {CommondataService} from '../commondata.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-searchbox',
  templateUrl: './searchbox.component.html',
  styleUrls: ['./searchbox.component.css']
})
export class SearchboxComponent implements OnInit {

  constructor(private router: Router,private commonDService:CommondataService) 
  { 

  }

  ngOnInit() 
  {
    if(!this.commonDService.getLoggedInStatus())
    {
      this.router.navigateByUrl('/login');      
    }
  }

}
