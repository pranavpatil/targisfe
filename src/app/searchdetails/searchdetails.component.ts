import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {HttpClient,HttpResponse } from "@angular/common/http";
import {baseUrl} from '../constants';
import {CommondataService,UserInformationModel} from '../commondata.service';



@Component({
  selector: 'app-searchdetails',
  templateUrl: './searchdetails.component.html',
styleUrls: [
  //'./searchdetails.component.css',
  '../assets/global/plugins/font-awesome/css/font-awesome.css','../assets/global/plugins/simple-line-icons/simple-line-icons.css','../assets/global/plugins/bootstrap/css/bootstrap.css','../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css',
  '../assets/global/plugins/select2/css/select2.css',
  '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
  '../assets/global/css/components.css',
  '../assets/global/css/plugins.css',
  '../assets/pages/css/login-5.css'

  
  ]
})
export class SearchdetailsComponent implements OnInit {

  searchDetailsurl = baseUrl + '/getParcelById'; 
  searchDetailsResponse: any = [];  

  constructor(private router: Router,private route: ActivatedRoute,private http:HttpClient,private commonDService:CommondataService) 
  {
  }

  ngOnInit() 
  {

    if(!this.commonDService.getLoggedInStatus())
    {
      this.router.navigateByUrl('/login');      
    }
        
    let loggedInUserInfo:UserInformationModel;
    loggedInUserInfo = this.commonDService.getUserInformation();
    
    const id = this.route.snapshot.paramMap.get('idx');
    this.searchDetailsurl = this.searchDetailsurl + "?parcelId=" + id + "&userId="+loggedInUserInfo.user_id;

    this.http.get(this.searchDetailsurl)
	  .subscribe((response: HttpResponse<any>) => 
	  {     
    
		  this.searchDetailsResponse = response ; //response;
		  console.log(response); 	
	  });	  

  }
  
  navigateBack()
  {
	 this.router.navigateByUrl('/searchbox');	  
  }

}
