import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchoutputComponent } from './searchoutput.component';

describe('SearchoutputComponent', () => {
  let component: SearchoutputComponent;
  let fixture: ComponentFixture<SearchoutputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchoutputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchoutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
