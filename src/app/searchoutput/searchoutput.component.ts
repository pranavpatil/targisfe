import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-searchoutput',
  templateUrl: './searchoutput.component.html',
  styleUrls: [
  './searchoutput.component.css',
  '../assets/global/plugins/font-awesome/css/font-awesome.css','../assets/global/plugins/simple-line-icons/simple-line-icons.css','../assets/global/plugins/bootstrap/css/bootstrap.css','../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css',
  '../assets/global/plugins/select2/css/select2.css',
  '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
  '../assets/global/css/components.css',
  '../assets/global/css/plugins.css',
  '../assets/pages/css/login-5.css'
 
  ]
})
export class SearchoutputComponent implements OnInit {
  public data : any;
  constructor(private router: Router) 
  { 
  }

 ngOnInit() 
 {
    /*this.data = 
  [{'name':'Anil', 'anil.singh581@gmail.com' :'ssd', 'age' :'34', 'city':'Noida, UP, India' },
    {'name':'Anil', 'email' :'anil.singh581@gmail.com', 'age' :'34', 'city':'Noida' },
    {'name':'Sunil', 'email' :'anil.singh581@gmail.com', 'age' :'34', 'city':'Noida' },
    {'name':'Alok', 'email' :'anil.singh581@gmail.com', 'age' :'34', 'city':'Noida' },
    {'name':'Tinku', 'email' :'anil.singh581@gmail.com', 'age' :'34', 'city':'Noida' },
    {'name':'XYZ', 'email' :'anil.singh581@gmail.com', 'age' :'34', 'city':'Noida' },
    {'name':'asas', 'email' :'anil.singh581@gmail.com', 'age' :'34', 'city':'Noida' },
    {'name':'erer', 'email' :'anil.singh581@gmail.com', 'age' :'34', 'city':'Noida' },
    {'name':'jhjh', 'email' :'anil.singh581@gmail.com', 'age' :'34', 'city':'Noida' }
   ]*/
   
   /*
    <td>{{searchResult.totalpoints | lowercase}}</td>	  
     <td>{{searchResult.fulladdress | lowercase}}</td>		 
     <td>{{searchResult.jurisdiction | lowercase}}</td>		 	 
     <td>{{searchResult.sumallcards | lowercase}}</td>		 	 	 
     <td>{{searchResult.saleinstrumenttype1 | lowercase}}</td>		 	 	 
     <td>{{searchResult.salevaliditydescription1 | lowercase}}</td>		 	 	 
     <td>{{searchResult.saledateymd1 | lowercase}}</td>		 	 	 
     <td>{{searchResult.saleprice1 | lowercase}}</td>		 	 	 
     <td>{{searchResult.saledateymd2 | lowercase}}</td>		 	 	 
     <td>{{searchResult.saleprice2 | lowercase}}</td>	   
   
   */
   
   this.data = 
  [
    {'totalpoints':'NA', 'fulladdress' :'NA', 'jurisdiction' :'NA', 'sumallcards':'NA', 'saleinstrumenttype1' :'NA', 'salevaliditydescription1' :'NA', 'saledateymd1' :'NA', 'saleprice1' :'NA', 'saledateymd2' :'NA','saleprice2' :'NA'}
  ]  
   
  
  }
  
  showDetailsScreen(idx)
  {
	     //alert(idx);
	  	 this.router.navigateByUrl('/searchdetails/'+idx);
  }
  
  refreshSearchResults(jsonMessage)
  {
	  this.data = jsonMessage;
	  console.log(this.data);
  }

}
