import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargisheaderComponent } from './targisheader.component';

describe('TargisheaderComponent', () => {
  let component: TargisheaderComponent;
  let fixture: ComponentFixture<TargisheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargisheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargisheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
