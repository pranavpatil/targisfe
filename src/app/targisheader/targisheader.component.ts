import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CommondataService,UserInformationModel} from '../commondata.service';

@Component({
  selector: 'app-targisheader',
  templateUrl: './targisheader.component.html',
  styleUrls: [
    '../assets/global/plugins/font-awesome/css/font-awesome.css','../assets/global/plugins/simple-line-icons/simple-line-icons.css','../assets/global/plugins/bootstrap/css/bootstrap.css','../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css',
    '../assets/global/plugins/select2/css/select2.css',
    '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
    '../assets/global/css/components.css',
    '../assets/global/css/plugins.css',
    '../assets/pages/css/login-5.css'
   
    ]
})
export class TargisheaderComponent implements OnInit {

  private isAdmin:string ="0";
  constructor(private router: Router,private commonDService:CommondataService) 
  { }

  ngOnInit() 
  {
		let loggedInUserInfo:UserInformationModel;
    loggedInUserInfo = this.commonDService.getUserInformation();
    this.isAdmin=loggedInUserInfo.user_admin;
  }

}
