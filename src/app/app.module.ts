import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent }         from './app.component';
//import { DashboardComponent }   from './dashboard/dashboard.component';
//import { HeroDetailComponent }  from './hero-detail/hero-detail.component';
//import { HeroesComponent }      from './heroes/heroes.component';
//import { HeroSearchComponent }  from './hero-search/hero-search.component';
//import { MessagesComponent }    from './messages/messages.component';
import { LoginComponent } from './login/login.component';
import { UploadfileComponent } from './uploadfile/uploadfile.component';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { SearchpanelComponent } from './searchpanel/searchpanel.component';
import { SearchoutputComponent } from './searchoutput/searchoutput.component';
import { SearchboxComponent } from './searchbox/searchbox.component';
import { PointsconfigurationComponent } from './pointsconfiguration/pointsconfiguration.component';
import { SearchdetailsComponent } from './searchdetails/searchdetails.component';
import {DataTableModule} from "angular-6-datatable";
import { UserregistrationComponent } from './userregistration/userregistration.component';
import { TargisheaderComponent } from './targisheader/targisheader.component';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
 // Change this to your upload POST address:
  url: 'https://httpbin.org/post',
  maxFilesize: 50,
  acceptedFiles: 'image/*'
};

const baseUrl = "http://192.168.0.101:8080";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
	DropzoneModule,
	DataTableModule
  ],
  declarations: [
    AppComponent,
    //DashboardComponent,
    //HeroesComponent,
    //HeroDetailComponent,
    //MessagesComponent,
    //HeroSearchComponent,
    LoginComponent,
    UploadfileComponent,
    SearchpanelComponent,
    SearchoutputComponent,
    SearchboxComponent,
    PointsconfigurationComponent,
    SearchdetailsComponent,
    UserregistrationComponent,
    TargisheaderComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
