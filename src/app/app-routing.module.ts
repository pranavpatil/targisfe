import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//import { DashboardComponent }   from './dashboard/dashboard.component';
//import { HeroesComponent }      from './heroes/heroes.component';
//import { HeroDetailComponent }  from './hero-detail/hero-detail.component';
import { LoginComponent }  from './login/login.component';
import { UploadfileComponent }  from './uploadfile/uploadfile.component';
import { SearchboxComponent } from './searchbox/searchbox.component';
import { PointsconfigurationComponent } from './pointsconfiguration/pointsconfiguration.component';
import { SearchdetailsComponent } from './searchdetails/searchdetails.component';
import { UserregistrationComponent } from './userregistration/userregistration.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
//  { path: 'dashboard', component: DashboardComponent },
//  { path: 'detail/:id', component: HeroDetailComponent },
//  { path: 'heroes', component: HeroesComponent },
  { path: 'login', component: LoginComponent } ,
  { path: 'uploadfile', component: UploadfileComponent },   
  { path: 'searchbox', component: SearchboxComponent },   
  { path: 'pointsconfiguration', component: PointsconfigurationComponent },
  { path: 'searchdetails/:idx', component: SearchdetailsComponent },   
  { path: 'userregistration', component: UserregistrationComponent }   
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
