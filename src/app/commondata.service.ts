import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class CommondataService 
{
  private userInformation : UserInformationModel;
  isLoggedIn = false;
  constructor() 
  {

  }

  setLoggedInStatus(loggedInStatus)
  {
    this.isLoggedIn = loggedInStatus;
  }

  getLoggedInStatus()
  {
    return this.isLoggedIn;
  }

  setUserInformation(userInformation)
  {
    this.userInformation = userInformation;
  }

  getUserInformation()
  {
    return this.userInformation;
  }
}

export interface UserInformationModel
{
    user_id: string;
    user_login : string;
    user_email : string;
    user_first_name : string;
    user_last_name : string;
    user_admin:string;
    user_password:string;

}

