import { Component, OnInit,Input } from '@angular/core';
import {HttpClient,HttpResponse } from "@angular/common/http";
import { Http, Response } from '@angular/http';
import {baseUrl} from '../constants';
import { SearchoutputComponent } from '../searchoutput/searchoutput.component';
import { Router } from '@angular/router';
import {CommondataService,UserInformationModel} from '../commondata.service';

@Component({
  selector: 'app-searchpanel',
  templateUrl: './searchpanel.component.html',
  styleUrls: ['../assets/global/plugins/font-awesome/css/font-awesome.css','../assets/global/plugins/simple-line-icons/simple-line-icons.css','../assets/global/plugins/bootstrap/css/bootstrap.css','../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css',
  '../assets/global/plugins/select2/css/select2.css',
  '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
  '../assets/global/css/components.css',
  '../assets/global/css/plugins.css',
  '../assets/pages/css/login-5.css'
  ]
})
export class SearchpanelComponent implements OnInit {

  @Input() searchOutput: SearchoutputComponent;

  

  getHeaderUrl = baseUrl + '/getHeader';
  searchUrl = baseUrl + '/searchParcelValue';
  saveSearchUrl = baseUrl + '/saveSearch';
  
  csvHeaders: any = []; 
  csvSearchResult: any = []; 
	saveSearchResult: any; 
	getPointsConfigurationUrl = baseUrl + '/getSearchCriteria';
	getPointsConfigurationResult: any; 
	searchcriteria : any;
	user : any = [];
	userName = "Guest";
  constructor(private http:HttpClient,private router: Router,private commonDService:CommondataService) 
  { }

  ngOnInit() 
  {

		this.searchcriteria = 
		[
		{     criteriaName: '',     criteriaCondition: '' , criteriaValue:''   },
		{     criteriaName: '',     criteriaCondition: '' , criteriaValue:''   },
		{     criteriaName: '',     criteriaCondition: '' , criteriaValue:''   },
		{     criteriaName: '',     criteriaCondition: '' , criteriaValue:''   },
		{     criteriaName: '',     criteriaCondition: '' , criteriaValue:''   }
		]


	  this.http.get(this.getHeaderUrl)
	  .subscribe((response: HttpResponse<any>) => 
	  {     
		//console.log(response); // logs 200     
		this.csvHeaders = Object.values(response) ; //response;
	  });
		
		let loggedInUserInfo:UserInformationModel;
		loggedInUserInfo = this.commonDService.getUserInformation();
		this.userName = loggedInUserInfo.user_email;
	  this.getPointsConfigurationUrl = this.getPointsConfigurationUrl + "?userId="+loggedInUserInfo.user_id;
	  this.http.get(this.getPointsConfigurationUrl)
	  .subscribe((response: HttpResponse<any>) => 
	  {     
		  console.log(response); // logs 200
		  this.getPointsConfigurationResult =response ; //response;
		  if(response){
			this.searchcriteria=response;
			
		  }
	  });	 		


  }
  
  saveSearchClicked()
  {
		let loggedInUserInfo:UserInformationModel;
		loggedInUserInfo = this.commonDService.getUserInformation();		
	  var searchRequestPayLoad = {"savesearch": this.searchcriteria, "userId" : loggedInUserInfo.user_id};
	  var searchRequestPayLoadStringified = JSON.stringify(searchRequestPayLoad);
	  
	  this.http.post(this.saveSearchUrl, searchRequestPayLoadStringified)
	  .subscribe((response: HttpResponse<any>) => 
	  {     
			this.saveSearchResult = response;
			alert("Successfully Saved search criteria");
	  });	  	 
  }

  searchClicked()
  {
		let loggedInUserInfo:UserInformationModel;
		loggedInUserInfo = this.commonDService.getUserInformation();	
	  console.log('SearchClicked' + this.searchcriteria[4].criteriaName + this.searchcriteria[4].criteriaCondition + this.searchcriteria[4].criteriaValue);
	  
	  /*{"search":
	  [{"criteriaName":"GovID","criteriaCondition":"Equals","criteriaValue":"2568"},
	  {"criteriaName":"","criteriaCondition":"","criteriaValue":""},
	  {"criteriaName":"","criteriaCondition":"","criteriaValue":""},
	  {"criteriaName":"","criteriaCondition":"","criteriaValue":""},
	  {"criteriaName":"","criteriaCondition":"","criteriaValue":""}]}"*/
      //var searchRequestPayLoad = '{"search":[{"header":"4","criteria":"=","value":"MOSES FAMILY"}]}';
	  
	  var searchRequestPayLoad = {"search": this.searchcriteria, "userId" : loggedInUserInfo.user_id};
	  var searchRequestPayLoadStringified = JSON.stringify(searchRequestPayLoad);
	  
	  this.http.post(this.searchUrl, searchRequestPayLoadStringified)
	  .subscribe((response: HttpResponse<any>) => 
	  {     
		//console.log(response.status); // logs 200     
		//console.log(response.headers); // logs []
		this.csvSearchResult = Object.values(response);
		//console.log(this.csvSearchResult);	
		
  let data = 
  [
    {'totalpoints':'tp1changed', 'csv_dumpcol33' :'fad2', 'csv_dumpcol45' :'jur3', 'sumallcards':'sum4', 'saleinstrumenttype1' :'sale5', 'salevaliditydescription1' :'svalidity6', 'saledateymd1' :'saledate7', 'saleprice1' :'sprice8', 'saledateymd2' :'saledate9','saleprice2' :'sp10'},
    {'totalpoints':'tp21changed', 'fulladdress' :'fad22', 'jurisdiction' :'jur23', 'sumallcards':'sum24', 'saleinstrumenttype1' :'sale25', 'salevaliditydescription1' :'svalidity26', 'saledateymd1' :'saledate27', 'saleprice1' :'sprice28', 'saledateymd2' :'saledate29','saleprice2' :'sp210'}

  ] 	
  
		this.searchOutput.refreshSearchResults(this.csvSearchResult);
		//this.user.username = '';
		//this.jsonResponse = response.json;
		
	  });	  
	  
  }
 
	pointsConfigurationClicked()
	{
		this.router.navigateByUrl('/pointsconfiguration');
	}
//export interface SearchCriteria {   criteriaName: string;   criteriaCondition: string; criteriaValue:string } 
  
}
